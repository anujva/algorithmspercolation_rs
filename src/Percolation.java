
public class Percolation {
	private class Coordinate{
		public int row;
		public int column;
	}
	
	private class Site{
		public boolean open;
		public Coordinate rootIndex;
		public static final boolean OPEN = true;
		public static final boolean CLOSED = false;
		
		Site(){
			open = false;
			rootIndex = new Coordinate();
		}
	}
	
	private Site grid[][];
	private WeightedQuickUnionUF sites;
	private int numberOfSites;
	
	public Percolation(int N){
		grid = new Site[N][N];
		for(int i=0; i<N; i++){
			for(int j=0; j<N; j++){
				grid[i][j] = new Site();
			}
		}
		for(int i=0; i<N; i++){
			for(int j=0; j<N; j++){
				grid[i][j].open = Site.CLOSED;
				grid[i][j].rootIndex.row = i;
				grid[i][j].rootIndex.column = j;
				sites = new WeightedQuickUnionUF(N*N); //we will translate row, column as N*row + col = index and vice versa
				numberOfSites = N;
			}
		}
	}
	
	public void open(int i, int j){
		i=i-1;
		j=j-1;
		if(i<0 || j<0 || i>numberOfSites-1 || j>numberOfSites-1){
			throw new java.lang.IndexOutOfBoundsException("Index out of bounds");
		}
		int sitenumber = numberOfSites*i+j;
		grid[i][j].open = Site.OPEN;
		for(int p=-1; p<2; p++){
			for(int q=-1; q<2; q++){
				if(p==q || p==-1*q){
					continue;
				}
				if(i+p<0 || j+q<0 || i+p > numberOfSites -1 || j+q > numberOfSites -1){
					continue;
				}
				try{
				if(grid[i+p][j+q].open == Site.OPEN){
					int site_neighbour = (i+p)*numberOfSites+j+q;
					sites.union(site_neighbour, sitenumber);
				}
				}catch(Exception e){
					System.out.println("Error" + (i+p));
					System.out.println("Error" + ( j+q));
				}
			}
		}
	}
	
	public boolean isOpen(int i, int j){
		i=i-1;
		j=j-1;
		if(i<0 || j<0 || i>numberOfSites-1 || j>numberOfSites-1){
			throw new java.lang.IndexOutOfBoundsException("Index out of bounds");
		}
		if(grid[i][j].open == Site.OPEN)
			return true;
		else
			return false;
	}
	
	public boolean isFull(int i, int j){
		i=i-1;
		j=j-1;
		if(i<0 || j<0 || i>numberOfSites-1 || j>numberOfSites-1){
			throw new java.lang.IndexOutOfBoundsException("Index out of bounds");
		}
		//check if its connected to any of the top open sites
		if(grid[i][j].open == Site.CLOSED){
			//System.out.println("The site is closed and hence not full");
			return false;
		}else{
			int sitenumber = numberOfSites*i+j;
			for(int col = 0; col<numberOfSites; col++){
				if(grid[0][col].open == Site.OPEN){
					if(sites.connected(col, sitenumber)){
						//System.out.println("The site is full");
						return true;
					}
				}
			}
		}
		return false;
	}
	
	public boolean percolates(){
		//check if any of the bottom row is connected to any of the top row
		if(numberOfSites == 1){
			if(grid[0][0].open == Site.OPEN)
				return true;
			else 
				return false;
		}
		for(int bottomrow = numberOfSites*numberOfSites-1; bottomrow>(numberOfSites*numberOfSites)-1-numberOfSites; bottomrow--){
			for(int toprow = 0; toprow<numberOfSites; toprow++){
				if(sites.connected(bottomrow, toprow)){
					return true;
				}
			}
		}
		return false;
	}
	
	public static void main(String args[]){
		Percolation pc = new Percolation(2);
		System.out.println(pc.percolates());
		pc.open(1, 1);
		System.out.println(pc.percolates());
		pc.open(1, 2);
		System.out.println(pc.percolates());
		pc.open(2, 2);
		System.out.println(pc.percolates());
		/*pc.open(1, 2);
		System.out.println(pc.isFull(1, 3));
		System.out.println(pc.sites.connected(1, 3));
		pc.open(1, 4);
		System.out.println(pc.isFull(1, 4));
		System.out.println(pc.percolates());
		pc.open(3, 2);
		pc.open(3, 3);
		System.out.println(pc.percolates());
		pc.open(4, 3);
		System.out.println(pc.percolates());
		pc.open(2, 2);
		System.out.println(pc.percolates());*/
	}
}

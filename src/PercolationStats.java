
public class PercolationStats {

	/**
	 * @param args
	 */
   private int valueOfN;
   private double[] numberOfRepeatedTrialsTillPercolation;
   private int valueOfT;
   
   private int random(){
	   //generate a random number between 0 - N-1
	   return StdRandom.uniform(valueOfN);
   }
   
   public PercolationStats(int N, int T){
	   // perform T independent computational experiments on an N-by-N grid
	   numberOfRepeatedTrialsTillPercolation = new double[T];
	   valueOfN = N;
	   valueOfT = T;
	   int row = 0;
	   int column = 0;
	   for(int i=0; i<T; i++){
		   Percolation pc = new Percolation(N);
		   int count_no = 0;
		   while(!pc.percolates()){
			   row = random();
			   column = random();
			   boolean foundAclosedsite = false;
			   while(!foundAclosedsite){
				   if(pc.isOpen(row+1, column+1)){
					   row = random();
					   column = random();
				   }else{
					   foundAclosedsite = true;
				   }
			   }
			 //we have random row and column that is not open so open it.. 
			   pc.open(row+1, column+1);
			   count_no++;
		   }
		   //System.out.println("The number of count: "+ count_no);
		   numberOfRepeatedTrialsTillPercolation[i] = (double)count_no/(double)(valueOfN*valueOfN);
	   }
   }
   
   public double mean(){
	   // sample mean of percolation threshold
	   double mean = StdStats.mean(numberOfRepeatedTrialsTillPercolation);
	   return mean;
   }
   
   public double stddev(){
	   // sample standard deviation of percolation threshold
	   double stddev = StdStats.stddev(numberOfRepeatedTrialsTillPercolation);
	   return stddev;
   }
   
   public double confidenceLo(){
	   // returns lower bound of the 95% confidence interval
	   double mean = mean();
	   double stddev = stddev();
	   double confiLo = mean - (1.96*Math.sqrt(stddev))/Math.sqrt(valueOfT);
	   return confiLo;
   }
   
   public double confidenceHi(){
		// returns upper bound of the 95% confidence interval
	   double mean = mean();
	   double stddev = stddev();
	   double confiHi = mean - (1.96*Math.sqrt(stddev))/Math.sqrt(valueOfT);
	   return confiHi;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int N = Integer.parseInt(args[0]);
		int T = Integer.parseInt(args[1]);
		PercolationStats pcStats = new PercolationStats(N, T);
		System.out.println("mean = "+ pcStats.mean());
		System.out.println("stddev = "+ pcStats.stddev());
		System.out.println("95% confidence interval = " + pcStats.confidenceLo() + ", " + pcStats.confidenceHi());
	}
}
